import QrScanner from "../js/qr-scanner/qr-scanner.min.js";
function setValues(qrarray){
  document.getElementById('server').value =  qrarray[0]
  document.getElementById('user').value =  qrarray[1]
  document.getElementById('unitname').value =  qrarray[3]
  document.getElementById('password').value =  qrarray[2]
}

function initQRScan(){
    QrScanner.WORKER_PATH = '../js/qr-scanner/qr-scanner-worker.min.js';

    /////////////////////CAM
    const video = document.getElementById('qr-video');
    const camHasCamera = document.getElementById('cam-has-camera');
    const camQrResult = document.getElementById('cam-qr-result');
    const camQrResultTimestamp = document.getElementById('cam-qr-result-timestamp');

    /////////////////////
    const fileSelector = document.getElementById('file-selector');
    const fileQrResult = document.getElementById('file-qr-result');

    function setResult(label, result) {
        label.textContent = result;
        label.style.color = 'teal';

        ////////////////////////////CAM
        camQrResultTimestamp.textContent = new Date().toString();
        label.style.color = 'teal';
        clearTimeout(label.highlightTimeout);
        label.highlightTimeout = setTimeout(() => label.style.color = 'inherit', 100);
        /////////////////////////////////////////
    }
    // ####### Web Cam Scanning #######

    QrScanner.hasCamera().then(hasCamera => camHasCamera.textContent = hasCamera);

    const scanner = new QrScanner(video, result => setResult(camQrResult, result));
    scanner.start();

    document.getElementById('inversion-mode-select').addEventListener('change', event => {
        scanner.setInversionMode(event.target.value);
    });

    // ####### File Scanning #######

    fileSelector.addEventListener('change', event => {
        const file = fileSelector.files[0];
        if (!file) {
            return;
        }
        QrScanner.scanImage(file)
            .then(result => {
              //setResult(fileQrResult, result)
              var qrarray = result.split(";")
              //alert(qrarray[0])
              setValues(qrarray)
            })
            .catch(e => setResult(fileQrResult, e || 'No QR code found.'));
    });
}

function init() {
  initQRScan()  
}

init();