if('serviceWorker' in navigator){
  navigator.serviceWorker.register('/sw.js')
    .then(reg => console.log('service worker registered', reg))
    .catch(err => console.log('service worker not registered', err));
}

function sendMessage(){
  modulemqtt.sendMessage();
}

function sendMessage1(){
  modulemqtt.sendMessage1();
}

function mqttsub(){
  modulemqtt.mqttsub();
}
function initConnection(url,user,password,unitname){
  modulemqtt.initConnection(url,user,password,unitname);
}
function getStatus(){
  modulemqtt.getStatus();
}

function init() {
  // Get the location list, and update the UI.
  //storeMQTTDetails()
  //alert(window.localStorage.getItem('mqtt_password'))
  var url = window.localStorage.getItem('mqtt_server')
  var user = window.localStorage.getItem('mqtt_user')
  var password = window.localStorage.getItem('mqtt_password')
  var unitname = window.localStorage.getItem('mqtt_unitname')

  //NEW
  //document.getElementById('butRefresh1').addEventListener('touchmove', sendMessage1)

  if ((url != null) & (user != null) & (password != null) & (unitname != null)){
      initConnection(url,user,password,unitname);
      getStatus();
      //mqttsub();
      document.getElementById('butRefresh').addEventListener('click', sendMessage);      
  }else{
    alert('Config needed')
  }
  
}

init();
