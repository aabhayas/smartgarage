function storeMQTTDetails(){
  var url = localStorage.getItem('mqtt_url');
  if(url == null){
    alert('Empty')
    window.localStorage.setItem('mqtt_url','abc')
  }else{
    alert(url)
  }
  //myStorage = window.localStorage;

}
function clearMQTTDetails(){
  localStorage.removeItem('mqtt_url');
}

function saveMQTTDetails(){
  var mqtt_password = document.forms["mqttconfig"]["password"].value
  window.localStorage.setItem('mqtt_password',document.forms["mqttconfig"]["password"].value)
  window.localStorage.setItem('mqtt_server',document.forms["mqttconfig"]["server"].value)
  window.localStorage.setItem('mqtt_user',document.forms["mqttconfig"]["user"].value)
  window.localStorage.setItem('mqtt_unitname',document.forms["mqttconfig"]["unitname"].value)

  //alert(mydata);
}

// function setValues(qrarray){
// //   document.getElementById('server').value =  qrarray[0]
// //   document.getElementById('user').value =  qrarray[1]
// //   document.getElementById('unitname').value =  qrarray[3]
// //   document.getElementById('password').value =  qrarray[2]
// }

function initQRScan(){
    QrScanner.WORKER_PATH = '../js/qr-scanner/qr-scanner-worker.min.js';

    const fileSelector = document.getElementById('file-selector');
    const fileQrResult = document.getElementById('file-qr-result');

    function setResult(label, result) {
        label.textContent = result;
        label.style.color = 'teal';
    }

    // ####### File Scanning #######

    fileSelector.addEventListener('change', event => {
        const file = fileSelector.files[0];
        if (!file) {
            return;
        }
        QrScanner.scanImage(file)
            .then(result => {
              setResult(fileQrResult, result)
              var qrarray = result.split(";")
              alert(qrarray[0])
              //setValues(qrarray)
            })
            .catch(e => setResult(fileQrResult, e || 'No QR code found.'));
    });
}

function init() {
  //alert(window.localStorage.getItem('mqtt_password',document.forms["mqttconfig"]["password"].value))
  document.getElementById('server').value =  window.localStorage.getItem('mqtt_server')
  document.getElementById('user').value =  window.localStorage.getItem('mqtt_user')
  document.getElementById('unitname').value =  window.localStorage.getItem('mqtt_unitname')
  document.getElementById('password').value =  window.localStorage.getItem('mqtt_password')
  //initQRScan()
  //document.getElementById('password').value =  window.localStorage.getItem('mqtt_password')

}

init();