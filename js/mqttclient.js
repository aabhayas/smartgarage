
var TOPIC = 'GRA000'
var ACTION = 'NONE'
var LAST_HEART_BEAT = new Date(1970,1,1)
const STATUS_CHECK_DURATION = 3000
const HOST = 'ws://localhost:9001'
var client


this.initConnection = function(mqtt_host,mqtt_user,mqtt_password,mqtt_unitname) {
  var mqtt = require('mqtt')
  var clientId = 'mqttjs_' + Math.random().toString(16).substr(2, 8)
  var host = mqtt_host
  
  var options = {
    keepalive: 0,
    clientId: clientId,
    protocolId: 'MQTT',
    protocolVersion: 4,
    clean: true, 
    username: mqtt_user,
    password: mqtt_password,         
  } 
  client = mqtt.connect(host, options)
  client.on('connect', function () {
      TOPIC = mqtt_unitname
      mqttsub()
  })
}

this.getStatus = function(param){
  var myVar = setInterval(myTimer, STATUS_CHECK_DURATION);
  timediff = (new Date() - LAST_HEART_BEAT) / 1000
  if(timediff > 60){
    document.getElementById("butRefresh").src =  "img/icons/ic_unknown_garage.png"
    document.getElementById('label_action').innerText = "Status Unknown"
    document.getElementById('label_status').innerText = "Connecting to Smart Gateway"
  }else{

  }
  function myTimer() {
     client.publish(TOPIC, 'STATUS')
  }
}

// this.sendMessage = function(param) {
//   //var status = document.getElementById('butRefresh').innerText 
//   var status = document.getElementById("butRefresh").src
//   status = status.split(/[\\\/]/).pop();
//   if (status == "ic_close_garage_round.png"){
//     client.publish(TOPIC, 'R1ON')
//   }else if (status == "ic_open_garage_round.png"){
//     client.publish(TOPIC, 'R1OFF')
//   } 
// }

this.sendMessage = function(param) {
  var status = document.getElementById("butRefresh").src
  status = status.split(/[\\\/]/).pop();
  if (status == "ic_close_garage_round.png"){
      client.publish(TOPIC, 'R1ON')
      ACTION = 'OPEN'
  }else if (status == "ic_open_garage_round.png"){
      client.publish(TOPIC, 'R1OFF')
      ACTION = 'CLOSE'
  } 
  var button = document.getElementById("butRefresh");
  button.classList.add("blinkcss");
}

//NEW
// this.sendMessage1 = function(param) {
//   var status = document.getElementById("butRefresh").src
//   status = status.split(/[\\\/]/).pop();
//   // if (status == "ic_close_garage_round.png"){
//   //     client.publish(TOPIC, 'R1ON')
//   //     ACTION = 'OPEN'
//   // }else if (status == "ic_open_garage_round.png"){
//   //     client.publish(TOPIC, 'R1OFF')
//   //     ACTION = 'CLOSE'
//   // } 
//   var button = document.getElementById("butRefresh1");
//   button.classList.add("blinkcss");
// }


this.mqttsub = function(param) {
  client.subscribe(TOPIC, function (err) {
      if (!err) {
            //alert('Connected')
      }
  })
  client.on('message', function (topic, message, packet) {
  console.log('Received Message:= ' + message.toString() + '\nOn topic:= ' + topic)
  try {
    var status = JSON.parse(message.toString())
    if (status.R1 == 'ON'){
          LAST_HEART_BEAT = new Date()
          processOpen()
      }else if (status.R1 == 'OFF'){
          LAST_HEART_BEAT = new Date()
          processClose()
      }
    } catch(e){
    }    
  })

function processOpen() {
  if ( ACTION == 'OPEN'){
    var button = document.getElementById("butRefresh");
    button.classList.remove("blinkcss");
    ACTION = 'NONE'
    //NEW
    //var button1 = document.getElementById("butRefresh1");
    //button1.classList.remove("blinkcss");
  }

  document.getElementById("butRefresh").src =  "img/icons/ic_open_garage_round.png"
  document.getElementById('label_action').innerText = "Close Door"
  document.getElementById('label_status').innerText = "Your garage door is Opened" 
  //NEW
  //document.getElementById("butRefresh1").src =  "img/icons/open_garage.png"
}

function processClose() {
  if ( ACTION == 'CLOSE'){
    var button = document.getElementById("butRefresh");
    button.classList.remove("blinkcss");
    ACTION = 'NONE'
    //NEW
    //var button1 = document.getElementById("butRefresh1");
    //button1.classList.remove("blinkcss");
  }    
  //document.getElementById('butRefresh').innerText = "lock_outline"
  document.getElementById("butRefresh").src =  "img/icons/ic_close_garage_round.png"
  document.getElementById('label_action').innerText = "Open Door"
  document.getElementById('label_status').innerText = "Your garage door is Closed"      
  //NEW
  //document.getElementById("butRefresh1").src =  "img/icons/close_garage.png"
}
}  